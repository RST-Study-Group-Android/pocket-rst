package pl.com.rst.pocket.db;


import android.content.Context;

/**
 * Created by mpastuszka on 28.02.16.
 */
public class DbManager {
    private static DbManager instance;
    private final DbHelper dbHelper;

    private DbManager(Context context) {
        dbHelper = new DbHelper(context);
    }

    public final static void init(Context context) {
        synchronized (DbManager.class) {
            if(instance == null) {
                synchronized (DbManager.class) {
                    instance = new DbManager(context);
                }
            }
        }
    }

    public final static DbManager getInstance() {
        return instance;
    }
}
