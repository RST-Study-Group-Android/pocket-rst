package pl.com.rst.pocket.utils;

import android.util.SparseBooleanArray;
import android.widget.ListView;

import java.util.ArrayList;

import pl.com.rst.retrofit.rest.target_process.objects.IId;

/**
 * Created by wierzchanowskig on 21.02.2016.
 */
public class MultipleChoiceListViewHelper {

    public static ArrayList<String> getSelectedItemsIdList(ListView listView) {
        ArrayList<String> ids = new ArrayList<>();
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        for (int i = 0; i < checked.size(); i++) {
            if(checked.valueAt(i)) {
                IId project = (IId) listView.getItemAtPosition(checked.keyAt(i));
                ids.add(project.getId());
            }
        }
        return ids;
    }
}
