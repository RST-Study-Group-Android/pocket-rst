package pl.com.rst.pocket.db.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import pl.com.rst.pocket.db.DbHelper;

/**
 * Created by mpastuszka on 06.03.16.
 */
public abstract class ContentHelper {
    private final DbHelper mDbHelper;
    ContentHelper(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }
    public abstract Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder);
    public abstract String getType(Uri uri);
    public abstract Uri insert(Uri uri, ContentValues values);
    public abstract int delete(Uri uri, String selection, String[] selectionArgs);
    public abstract int update(Uri uri, ContentValues values, String selection, String[] selectionArgs);
    public abstract boolean isMath(Uri uri);

    protected DbHelper getDbHelper() {
        return mDbHelper;
    }
}
