package pl.com.rst.pocket.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import pl.com.rst.pocket.db.helper.ContentHelper;
import pl.com.rst.pocket.db.helper.SelectedValuesHelper;

/**
 * Created by mpastuszka on 06.03.16.
 */
public class PocketContentProvider extends ContentProvider {

    public static final String AUTHORITY = "pl.com.rst.pocket";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    private DbHelper mDbHelper;
    private final List<ContentHelper> mContentHelpers = new ArrayList<>();

    @Override
    public boolean onCreate() {
        mDbHelper = new DbHelper(getContext());
        mContentHelpers.add(new SelectedValuesHelper(mDbHelper));
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        for(ContentHelper helper: mContentHelpers) {
            if(helper.isMath(uri)) {
                cursor = helper.query(uri, projection, selection, selectionArgs, sortOrder);
            }
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String result = null;
        for(ContentHelper helper: mContentHelpers) {
            if(helper.isMath(uri)) {
                result = helper.getType(uri);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri result = null;
        for(ContentHelper helper: mContentHelpers) {
            if(helper.isMath(uri)) {
                result = helper.insert(uri, values);
            }
        }
        if(result != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return result;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int result = 0;
        for(ContentHelper helper: mContentHelpers) {
            if(helper.isMath(uri)) {
                result = helper.delete(uri, selection, selectionArgs);
            }
        }
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int result = 0;
        for(ContentHelper helper: mContentHelpers) {
            if(helper.isMath(uri)) {
                result = helper.update(uri, values, selection, selectionArgs);
            }
        }
        return result;
    }
}
