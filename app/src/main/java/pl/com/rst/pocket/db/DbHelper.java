package pl.com.rst.pocket.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import pl.com.rst.pocket.db.model.SelectedValue;

/**
 * Created by mpastuszka on 28.02.16.
 */
public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "rst-pocket";
    public static final int DB_VERSION = 1;


    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SelectedValue.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
