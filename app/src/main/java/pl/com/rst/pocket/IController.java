package pl.com.rst.pocket;

import android.support.v4.app.Fragment;

import pl.com.rst.retrofit.Credentials;

/**
 * Created by user_konferencyjna on 2016-02-08.
 */
public interface IController {
    void setCredentials(Credentials credentials);
    Credentials getCredentials();
    void changeFragment(Fragment fragment, boolean addToBackStack);
    void changeTitle(int stringId);
}
