package pl.com.rst.pocket;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

import pl.com.rst.pocket.db.model.SelectedValue;
import pl.com.rst.pocket.fragments.LoginFragment;
import pl.com.rst.retrofit.Credentials;

public class MainActivity extends AppCompatActivity implements IController {

    private Credentials mCredentials;
    private CacheQueryHandler mCacheQueryHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            changeFragment(LoginFragment.newInstance(), false);
        }
    }

    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_container, fragment);
        if(addToBackStack)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @Override
    public void changeTitle(int stringId) {
        setTitle(stringId);
    }

    @Override
    public void setCredentials(Credentials credentials) {
        mCredentials = credentials;
    }

    @Override
    public Credentials getCredentials() {
        return mCredentials;
    }

    private static class CacheQueryHandler extends AsyncQueryHandler {

        public static final int QUERY_SELECTED_VALUES = 1001;

        private final WeakReference<MainActivity> mMainActivity;

        public CacheQueryHandler(MainActivity mainActivity) {
            super(mainActivity.getContentResolver());
            mMainActivity = new WeakReference<>(mainActivity);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            switch (token) {
                case QUERY_SELECTED_VALUES:
                    if(mMainActivity.get() == null) {
                        return;
                    }
                    MainActivity mainActivity = mMainActivity.get();
                    LoginFragment loginFragment = LoginFragment.newInstance();
                    mainActivity.changeFragment(loginFragment, false);
                    break;
                default:
                    break;
            }
        }
    }
}