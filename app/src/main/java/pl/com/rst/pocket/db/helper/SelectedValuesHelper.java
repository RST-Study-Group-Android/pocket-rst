package pl.com.rst.pocket.db.helper;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import java.net.URI;

import pl.com.rst.pocket.db.DbHelper;
import pl.com.rst.pocket.db.PocketContentProvider;
import pl.com.rst.pocket.db.model.SelectedValue;

/**
 * Created by mpastuszka on 06.03.16.
 */
public class SelectedValuesHelper extends ContentHelper {

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int SELECTED_VALUES = 1;
    private static final int SELECTED_VALUES_ID = 2;

    static {
        URI_MATCHER.addURI(PocketContentProvider.AUTHORITY, SelectedValue.TABLE_NAME, SELECTED_VALUES);
        URI_MATCHER.addURI(PocketContentProvider.AUTHORITY, SelectedValue.TABLE_NAME + "/#", SELECTED_VALUES_ID);
    }

    public SelectedValuesHelper(DbHelper dbHelper) {
        super(dbHelper);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        switch (URI_MATCHER.match(uri)) {
            case SELECTED_VALUES:
                cursor = getDbHelper().getReadableDatabase().query(SelectedValue.TABLE_NAME
                        , projection
                        , selection
                        , selectionArgs
                        , null
                        , null
                        , sortOrder);
                break;
            case SELECTED_VALUES_ID:
                String[] args = new String[] {uri.getPathSegments().get(1)};
                cursor = getDbHelper().getReadableDatabase().query(SelectedValue.TABLE_NAME, projection, SelectedValue.COLUMN_ID + " = ?", args, null, null, sortOrder);
                break;
            default:
                throw new IllegalStateException("No uri match");
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri result = null;
        switch (URI_MATCHER.match(uri)) {
            case SELECTED_VALUES:
                long id = getDbHelper().getWritableDatabase().insert(SelectedValue.TABLE_NAME, null, values);
                result = uri.withAppendedPath(uri, Long.toString(id));
                break;
        }
        return result;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public boolean isMath(Uri uri) {
        return URI_MATCHER.match(uri) != UriMatcher.NO_MATCH;
    }
}
