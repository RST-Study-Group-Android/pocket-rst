package pl.com.rst.pocket.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.com.rst.pocket.IController;
import pl.com.rst.pocket.R;
import pl.com.rst.retrofit.Credentials;
import pl.com.rst.retrofit.rest.ServiceGenerator;
import pl.com.rst.retrofit.rest.target_process.TpApiService;
import pl.com.rst.retrofit.rest.target_process.objects.Items;
import pl.com.rst.retrofit.rest.target_process.objects.Project;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginFragment extends Fragment{

    @Bind(R.id.userName)
    EditText userName;
    @Bind(R.id.password)
    EditText password;

    IController mController;
    TpApiService mTpService;
    private Credentials mCredentials;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mController = (IController) context;
        mController.changeTitle(R.string.actionbar_login_title);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCredentials = new Credentials(null, null, null, null, null);
        mTpService = ServiceGenerator.createService(TpApiService.class, mCredentials);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.loginBtn)
    void loginClick() {
        String un = userName.getText().toString();
        String pass = password.getText().toString();

        if (isEmpty(userName)){
            userName.setError(getString(R.string.no_user_text));
            return;
        }
        if(isEmpty(password)) {
            password.setError(getString(R.string.no_password_text));
            return;
        }

        login(un, pass);
    }

    private boolean isEmpty(EditText input) {
        return input.getText().length() == 0;
    }

    private void login(String userName, String password){
        mCredentials.setLogin(userName);
        mCredentials.setPassword(password);
        mTpService.getProjects().enqueue(new Callback<Items<Project>>() {
            @Override
            public void onResponse(Response<Items<Project>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    mController.setCredentials(mCredentials);
                    mController.changeFragment(ProjectsFragment.newInstance(), false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
