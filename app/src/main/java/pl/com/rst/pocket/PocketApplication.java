package pl.com.rst.pocket;

import android.app.Application;

import pl.com.rst.pocket.db.DbManager;

/**
 * Created by mpastuszka on 28.02.16.
 */
public class PocketApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DbManager.init(this);
    }
}
