package pl.com.rst.pocket.db.model;

import android.database.Cursor;
import android.net.Uri;

import pl.com.rst.pocket.db.PocketContentProvider;

/**
 * Created by mpastuszka on 28.02.16.
 */
public class SelectedValue {
    public static final String TABLE_NAME = "SelectedValues";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ID_ELEMENT = "idElement";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_CHECKED = "checked";

    public static final Uri URI = Uri.withAppendedPath(PocketContentProvider.CONTENT_URI, TABLE_NAME);

    public static final class Types {
        public static final char TEAM = 'T';
        public static final char PROJECT = 'P';
    }

    public int id;
    public int idElement;
    public char type;
    public boolean checked;

    public static final String createTable() {
        return String.format("CREATE TABLE IF NOT EXISTS %s ("
            + "%s INTEGER PRIMARY KEY AUTOINCREMENT "
            + ", %s INTEGER NOT NULL"
            + ", %s SMALLINT NOT NULL"
            + ", %s NCHAR(1) NOT NULL"
            + ")"
            , TABLE_NAME, COLUMN_ID, COLUMN_ID_ELEMENT, COLUMN_CHECKED, COLUMN_TYPE
        );
    }

    public static SelectedValue fromCursor(Cursor cursor) {
        SelectedValue selectedValue = new SelectedValue();
        selectedValue.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        selectedValue.idElement = cursor.getInt(cursor.getColumnIndex(COLUMN_ID_ELEMENT));
        selectedValue.type = cursor.getString (cursor.getColumnIndex(COLUMN_TYPE)).charAt(0);
        selectedValue.checked = cursor.getShort(cursor.getColumnIndex(COLUMN_CHECKED)) > 0;
        return selectedValue;
    }

}
