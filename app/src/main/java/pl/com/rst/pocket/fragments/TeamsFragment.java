package pl.com.rst.pocket.fragments;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.com.rst.pocket.IController;
import pl.com.rst.pocket.R;
import pl.com.rst.pocket.db.model.SelectedValue;
import pl.com.rst.pocket.utils.MultipleChoiceListViewHelper;
import pl.com.rst.retrofit.rest.ServiceGenerator;
import pl.com.rst.retrofit.rest.target_process.TpApiService;
import pl.com.rst.retrofit.rest.target_process.objects.Ids;
import pl.com.rst.retrofit.rest.target_process.objects.Items;
import pl.com.rst.retrofit.rest.target_process.objects.Team;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by user_konferencyjna on 2016-02-08.
 */
public class TeamsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private IController mController;

    public static TeamsFragment newInstance() {
        return new TeamsFragment();
    }

    @Bind(R.id.listView)
    ListView mListView;
    ProjectsAdapter mAdapter;

    TpApiService mTpService;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mController = (IController)context;
        mController.changeTitle(R.string.actionbar_projects_title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTpService = ServiceGenerator.createService(TpApiService.class, mController.getCredentials());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teams, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new ProjectsAdapter(getContext(), new ArrayList<Team>());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mTpService.getTeams().enqueue(new Callback<Items<Team>>() {
            @Override
            public void onResponse(Response<Items<Team>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Items<Team> items = response.body();
                    List<Team> itemsList = items.getItems();
                    mAdapter.addAll(itemsList);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Team item = mAdapter.getItem(position);
        Toast.makeText(getContext(), item.getName(), Toast.LENGTH_SHORT).show();

//        mListView.setItemChecked(position, !mListView.isItemChecked(position));
    }

    private static class ProjectsAdapter extends ArrayAdapter<Team> {

        public ProjectsAdapter(Context context, List<Team> objects) {
            super(context, android.R.layout.simple_list_item_multiple_choice, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.text1 = ButterKnife.findById(convertView, android.R.id.text1);
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder)convertView.getTag();
            }
            Team team = getItem(position);
            StringBuilder sb = new StringBuilder();
            sb.append("(").append(team.getId()).append(") ").append(team.getName());
            viewHolder.text1.setText(sb.toString());
            return convertView;
        }
    }

    private static class ViewHolder {
        CheckedTextView text1;
    }

    @OnClick(R.id.btn_next)
    void onNextButtonClick(){
        ArrayList<String> projectsArrayList = getArguments().getStringArrayList(ProjectsFragment.PROJECTS_IDS_ARG);
        ArrayList<String> teamsArrayList = MultipleChoiceListViewHelper.getSelectedItemsIdList(mListView);
        if(projectsArrayList != null && teamsArrayList != null){
            Ids projectIds = new Ids();
            for(String id : projectsArrayList){
                projectIds.add(Integer.parseInt(id));
            }

            Ids teamIds = new Ids();
            for(String id : teamsArrayList){
                teamIds.add(Integer.parseInt(id));
            }

            mTpService.getContext(projectIds, teamIds).enqueue(new Callback<pl.com.rst.retrofit.rest.target_process.objects.Context>() {
                @Override
                public void onResponse(Response<pl.com.rst.retrofit.rest.target_process.objects.Context> response, Retrofit retrofit) {
                    if(response.isSuccess()){
                        Log.d("RESPONSE",response.body().getAcid());
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });

        }
    }

    private void saveSelections(List<Integer> selectedValues) {

    }

    private static class TeamsAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<TeamsFragment> mTeamsFragment;

        public TeamsAsyncQueryHandler(ContentResolver cr, TeamsFragment teamsFragment) {
            super(cr);
            mTeamsFragment = new WeakReference<>(teamsFragment);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);
            if(cursor == null || mTeamsFragment.get() == null) {
                return;
            }
            TeamsFragment teamsFragment = mTeamsFragment.get();
            List<SelectedValue> selectedValues = new ArrayList<>();
            while(cursor.moveToNext()) {
                SelectedValue selectedValue = SelectedValue.fromCursor(cursor);
                selectedValues.add(selectedValue);
            }
        }
    }
}
