package pl.com.rst.pocket.fragments;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.com.rst.pocket.IController;
import pl.com.rst.pocket.R;
import pl.com.rst.pocket.db.model.SelectedValue;
import pl.com.rst.pocket.utils.MultipleChoiceListViewHelper;
import pl.com.rst.retrofit.rest.ServiceGenerator;
import pl.com.rst.retrofit.rest.target_process.TpApiService;
import pl.com.rst.retrofit.rest.target_process.objects.Items;
import pl.com.rst.retrofit.rest.target_process.objects.Project;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by user_konferencyjna on 2016-02-08.
 */
public class ProjectsFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String PROJECTS_IDS_ARG = "ProjectsIds";
    private IController mController;

    public static ProjectsFragment newInstance() {
        return new ProjectsFragment();
    }

    @Bind(R.id.listView)
    ListView mListView;
    ProjectsAdapter mAdapter;

    TpApiService mTpService;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mController = (IController)context;
        mController.changeTitle(R.string.actionbar_projects_title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTpService = ServiceGenerator.createService(TpApiService.class, mController.getCredentials());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_projects, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SelectedProjectsQueryHandler queryHandler = new SelectedProjectsQueryHandler(this, getActivity().getContentResolver());
        mAdapter = new ProjectsAdapter(getContext(), new ArrayList<Project>());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mTpService.getProjects().enqueue(new Callback<Items<Project>>() {
            @Override
            public void onResponse(Response<Items<Project>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Items<Project> items = response.body();
                    List<Project> itemsList = items.getItems();
                    mAdapter.addAll(itemsList);
                    mAdapter.notifyDataSetChanged();
                    queryHandler.startQuery(SelectedProjectsQueryHandler.SELECTED_PROJECTS_QUERY
                            , null
                            , SelectedValue.URI
                            , null
                            , SelectedValue.COLUMN_TYPE + " = ?"
                            , new String[] {Character.toString(SelectedValue.Types.PROJECT) }
                            , SelectedValue.COLUMN_ID_ELEMENT);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final SelectedProjectsQueryHandler queryHandler = new SelectedProjectsQueryHandler(this, getActivity().getContentResolver());
        Project item = mAdapter.getItem(position);
        ContentValues contentValues = new ContentValues();
        contentValues.put(SelectedValue.COLUMN_ID_ELEMENT, Integer.parseInt(item.getId()));
        contentValues.put(SelectedValue.COLUMN_TYPE, Character.toString(SelectedValue.Types.PROJECT));
        contentValues.put(SelectedValue.COLUMN_CHECKED, true);
        queryHandler.startInsert(SelectedProjectsQueryHandler.INSERT_SELECTED_PROJECT
                , null
                , SelectedValue.URI
                , contentValues);

        Toast.makeText(getContext(), item.getName(), Toast.LENGTH_SHORT).show();

//        mListView.setItemChecked(position, !mListView.isItemChecked(position));
    }

    private static class ProjectsAdapter extends ArrayAdapter<Project> {

        private final SparseArray<SelectedValue> mSelectedValues = new SparseArray<>();
        public ProjectsAdapter(Context context, List<Project> objects) {
            super(context, android.R.layout.simple_list_item_multiple_choice, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.text1 = ButterKnife.findById(convertView, android.R.id.text1);
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder)convertView.getTag();
            }
            Project project = getItem(position);
            StringBuilder sb = new StringBuilder();
            int id = Integer.parseInt(project.getId());
            if(mSelectedValues.indexOfKey(id) >= 0)
            {
                SelectedValue selectedValue = mSelectedValues.get(id);
                viewHolder.text1.setChecked(selectedValue.checked);
            }
            sb.append("(").append(project.getId()).append(") ").append(project.getName());
            viewHolder.text1.setText(sb.toString());
            return convertView;
        }

        public void updateSelected(SparseArray<SelectedValue> selectedValues) {
            mSelectedValues.clear();
            for(int i = 0; i < selectedValues.size();i++) {
                mSelectedValues.append(selectedValues.keyAt(i), selectedValues.valueAt(i));
            }
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder {
        CheckedTextView text1;
    }

    @OnClick(R.id.btn_next)
    void onNextButtonClick(){
        TeamsFragment teamsFragment = new TeamsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(PROJECTS_IDS_ARG, MultipleChoiceListViewHelper.getSelectedItemsIdList(mListView));
        teamsFragment.setArguments(args);
        mController.changeFragment(teamsFragment, true);
    }

    private static class SelectedProjectsQueryHandler extends AsyncQueryHandler {
        private final static int SELECTED_PROJECTS_QUERY = 1001;
        private final static int INSERT_SELECTED_PROJECT = 1002;
        private final WeakReference<ProjectsFragment> mProjectsFragment;
        public SelectedProjectsQueryHandler(ProjectsFragment fragment, ContentResolver cr) {
            super(cr);
            this.mProjectsFragment = new WeakReference<>(fragment);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if(mProjectsFragment.get() == null) {
                return;
            }
            ProjectsAdapter adapter = mProjectsFragment.get().mAdapter;
            switch (token) {
                case SELECTED_PROJECTS_QUERY:
                    if(cursor != null) {
                        SparseArray<SelectedValue> result = new SparseArray<>();
                        while(cursor.moveToNext()) {
                            SelectedValue selectedValue = SelectedValue.fromCursor(cursor);
                            result.append(selectedValue.idElement, selectedValue);
                        }
                        adapter.updateSelected(result);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
