package pl.com.rst.retrofit.rest.target_process;

import pl.com.rst.retrofit.rest.ApiHostUrl;
import pl.com.rst.retrofit.rest.target_process.objects.Context;
import pl.com.rst.retrofit.rest.target_process.objects.Ids;
import pl.com.rst.retrofit.rest.target_process.objects.Items;
import pl.com.rst.retrofit.rest.target_process.objects.Project;
import pl.com.rst.retrofit.rest.target_process.objects.Team;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

@ApiHostUrl("https://nope.tpondemand.com/Targetprocess/api/v1")
public interface TpApiService {

    @GET("/api/v1/Teams/?format=json&include=[Id,Name]")
    Call<Items<Team>> getTeams();

    @GET("/api/v1/Projects/?format=json&include=[Id,Name]")
    Call<Items<Project>> getProjects();

    @GET("/api/v1/Context/?format=json")
    Call<Context> getContext(@Query("projectIds") Ids projectIds, @Query("teamIds") Ids teamIds);
}
