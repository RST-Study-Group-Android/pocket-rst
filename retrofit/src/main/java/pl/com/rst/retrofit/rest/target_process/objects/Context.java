package pl.com.rst.retrofit.rest.target_process.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Context implements Serializable{
    @SerializedName("Acid")
    String acid;

    public Context(String acid) {
        this.acid = acid;
    }

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }
}
