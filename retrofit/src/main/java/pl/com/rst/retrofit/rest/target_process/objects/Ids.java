package pl.com.rst.retrofit.rest.target_process.objects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ids {
    List<Integer> ids;

    public Ids(){
        ids = new ArrayList<>();
    }

    public Ids add(int id){
        ids.add(id);
        return this;
    }

    @Override
    public String toString() {
        String idsString = new String();
        Iterator<Integer> iterator = ids.iterator();
        while(iterator.hasNext()){
            Integer id = iterator.next();
            idsString += id;
            if(iterator.hasNext())
                idsString += ",";
        }
        return idsString;
    }
}
