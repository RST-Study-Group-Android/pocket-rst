package pl.com.rst.retrofit.rest.target_process.objects;

import com.google.gson.annotations.SerializedName;

public class Project implements IId{
    @SerializedName("Id")
    String id;
    @SerializedName("Name")
    String name;

    public Project(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
