package pl.com.rst.retrofit.rest;

import android.util.Base64;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;

import pl.com.rst.retrofit.Credentials;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class ServiceGenerator {

    public static <S> S createService(final Class<S> serviceClass, final Credentials credentials) {

        ApiHostUrl annotation = serviceClass.getAnnotation(ApiHostUrl.class);
        if(annotation == null)
            throw new RuntimeException("No Api Host Url defined. @ApiHostUrl required");
        String apiHostUrl = annotation.value();
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(apiHostUrl)
                        .addConverterFactory(GsonConverterFactory.create());
        OkHttpClient httpClient = new OkHttpClient();
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logger);

        //Authorization header
        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                if(credentials != null) {
                    if((credentials.getAccessToken() == null || credentials.getAccessToken().length() == 0) &&
                            credentials.getLogin() != null && credentials.getPassword() != null){
                        String loginAndPass = credentials.getLogin() + ":" + credentials.getPassword();
                        String token = Base64.encodeToString(loginAndPass.getBytes(), Base64.DEFAULT);
                        credentials.setAccessToken(token);
                    }
                    String token = "Basic " + credentials.getAccessToken();
                    token = token.trim();
                    Request newRequest = originalRequest.newBuilder().header("Authorization", token).build();
                    return chain.proceed(newRequest);
                }
                return chain.proceed(originalRequest);
            }
        });

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
