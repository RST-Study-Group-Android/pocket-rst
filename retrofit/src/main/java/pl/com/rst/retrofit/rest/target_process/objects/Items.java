package pl.com.rst.retrofit.rest.target_process.objects;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Items<S> {
    @SerializedName("Items")
    List<S> items;

    public Items(List<S> items) {
        this.items = items;
    }

    public List<S> getItems() {
        return items;
    }

    public void setItems(List<S> items) {
        this.items = items;
    }
}
