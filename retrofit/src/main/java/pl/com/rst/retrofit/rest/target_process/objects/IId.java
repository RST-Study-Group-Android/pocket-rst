package pl.com.rst.retrofit.rest.target_process.objects;

/**
 * Created by wierzchanowskig on 21.02.2016.
 */
public interface IId {
    String getId();
}
